const version = 6;

const nombreCache = 'cache-gatitos-' + version;
const recursosEstaticos = [
  '/',
  'index.html',
  'offline.html',
  'css/estilos.css',
  'js/scripts.js',
  'img/gato-triste.jpg',
  'img/gato-default.jpg',
  'https://fonts.googleapis.com/css?family=Montserrat&display=swap',
  'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
];

self.addEventListener('install', e => {
  console.log("Vamos a precachear");
  e.waitUntil(
    caches.open(nombreCache).then(cache => {
      return cache.addAll(recursosEstaticos).then(() => {
        console.log("Se han precacheado los recursos");
        Promise.resolve();
      }).catch(() => console.log("No se han precacheado los recursos"));
    })
  )
});

self.addEventListener('activate', e => {
  e.waitUntil(
    caches.keys().then(nombresCaches => {
      return Promise.all(
        nombresCaches.filter(cache => cache !== nombreCache).map(cache => caches.delete(cache))
      )
    })
  )
});

self.addEventListener('fetch', e => {
  if (e.request.method === 'GET' && e.request.mode === 'navigate') {
    e.respondWith(
      fetch(e.request).catch(() => {
        return caches.open(nombreCache).then(cache => {
          return cache.match('/offline.html').then(resp => {
            if (resp) {
              return resp;
            } else {
              return new Response('Tío, todo mal', { headers: { 'Content-Type': 'text/html;charset=utf-8' } })
            }
          })
        })
      })
    )
  } else if (e.request.destination === 'font') {
    e.respondWith(
      caches.open(nombreCache).then(cache => {
        return cache.match(e.request).then(resp => {
          if (resp) {
            return resp;
          } else {
            return fetch(e.request).then(resp => {
              cache.put(e.request, resp);
              return resp;
            })
          }
        })
      })
    )
  } else if (e.request.destination !== 'image') {
    e.respondWith(
      caches.open(nombreCache).then(cache => {
        return cache.match(e.request);
      })
    )
  } else if (e.request.destination === 'image' && e.request.url.endsWith('gato-triste.jpg')) {
    e.respondWith(
      caches.open(nombreCache).then(cache => {
        return cache.match(e.request).then(resp => {
          if (resp) {
            return resp;
          } else {
            return fetch(e.request);
          }
        })
      })
    )
  } else if (e.request.destination === 'image') {
    e.respondWith(
      fetch(e.request)
        .then(resp => {
          if (!resp.ok) {
            return buscaGatoDefault();
          } else {
            return resp;
          }
        })
        .catch(() => buscaGatoDefault())
    );
  }
});

function buscaGatoDefault() {
  const gatoDefault = 'img/gato-default.jpg';
  return caches.open(nombreCache).then(cache => {
    return cache.match(gatoDefault).then(resp => {
      if (resp) {
        return resp;
      } else {
        return fetch(gatoDefault);
      }
    })
  })
}
